

**Interface Fractures III - Silicon** is an audio-visual performance composition written in SuperCollider, Processing and Renoise. All of the code used in the performance is published here under a GNU GPL v3 licence. It is currently unknown how the binary data will be published (images, samples). 

Premiere is on 15/sept 2015 at Slovenian Cinemateque.

This code is currently pushed to GitHub AND GitLab:  
https://gitlab.com/novadeviator/IF3Si/  
https://github.com/novadeviator/IF3Si/  

Links
-----
http://emanat.si/si/produkcija/razpoke-vmesnika-iii--silicij/  
https://www.facebook.com/events/789807521128475/  
http://nova.deviator.si   
http://deviator.si  
http://www.kinoteka.si  

