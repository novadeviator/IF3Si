/*

	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  tiles.pde
    
                                                                             */




// TILES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  /* TODO:
     - different zooms (random?)
     - better rotation
     - multiple instances of the same thing - with
       different texture & some parameters + parameterize

     - zoom variations
     - ?amount of speed variations accross tiles
   */

void tiles( boolean render, // should we render or not?
            int tilesBgHue, int tilesBgSat, int tilesBgBri,
            int tilesHue, int tilesSat, int tilesBri,
            //color bgfill, // backgorund color
            //!!color tilecolor, // tile color
            int huedist, // tile color distance for interpolation
            int blendMode, // blending mode of tiles
            int numx, // number of tiles on X
            int numy, // number of tiles on Y
            int texBank,
            int texNum,
            float texSpeedX,
            float texSpeedY,
            float overlap )
{
  if (render) {

    colorMode(HSB, 127, 127, 127, 127);
    blendMode(BLEND);
    color bgfill = color(tilesBgHue, tilesBgSat, tilesBgBri);
    color tilecolor = color(tilesHue, tilesSat, tilesBri);
    
    fill(bgfill);
    noStroke();
    rect(0, 0, width, height);
    switchBlendMode(blendMode); // blendMode function using integers

    texBank = min(texBank, imgPool.length - 1);
    texNum = min(texNum, imgPool[texBank].length - 1);

    // for (int i = 0; i < imgPool.length; i++) {
    //   if (texBank == i) {
        
    //   }
    // }


  
    texSpeedX *= 0.01;
    texSpeedY *= 0.01;
    texX += sq(texSpeedX); 
    texY += sq(texSpeedY); 

    numx = max(1, numx);
    numy = max(1, numy);
    float numxfact = 1 / float(numx);
    float numyfact = 1 / float(numy);
    float aspectRatio = (height / float(numy)) / (width / float(numx));
 
    float offsetPerc = overlap * (300/127);

    float uniZoom = numxfact * (float(width)/1024);
  
    float offsetX = width * numxfact * offsetPerc * 0.01;
    float offsetY = height * numyfact * offsetPerc * 0.01;
    float texoffsetX =   1 + 0.01 * offsetPerc ;
    float texoffsetY =  texoffsetX;

    float huefactor = float(huedist) / (numx * numy);

    for (int nx=0; nx < numx; nx++) {

      for (int ny=0; ny < numy; ny++) {

        int tileID = nx * numy + ny;
        float randX = rands[ (tileID % rands.length) ] * 0.01;
        float randY = rands[ ((tileID + 50) % rands.length) ] * 0.01;
        float newhue = hue(tilecolor) + huefactor * tileID;
        tint(newhue, saturation(tilecolor), brightness(tilecolor), alpha(tilecolor));
        textureWrap(REPEAT);
        textureMode(NORMAL);

        beginShape();
      
        texture(imgPool[texBank][texNum]);
        vertex(width * numxfact *  nx      - offsetX, height * numyfact *  ny      - offsetY, 0                        + texX * randX, 0                                  + texY * randY);
        vertex(width * numxfact * (nx + 1) + offsetX, height * numyfact *  ny      - offsetY, 1 * uniZoom * texoffsetX + texX * randX, 0                                  + texY * randY);
        vertex(width * numxfact * (nx + 1) + offsetX, height * numyfact * (ny + 1) + offsetY, 1 * uniZoom * texoffsetX + texX * randX, aspectRatio * uniZoom * texoffsetY + texY * randY);
        vertex(width * numxfact *  nx      - offsetX, height * numyfact * (ny + 1) + offsetY, 0                        + texX * randX, aspectRatio * uniZoom * texoffsetY + texY * randY);

        endShape();

      }
    }
  }
}




