/*
	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  functions.pde
                                                          */



// function to shuffle an array of integers
void shuffle(int[] a)
{  
  int temp, pick;
 
   for(int i=0; i<a.length; i++)
   {
     temp = a[i];
     pick = (int)random(a.length);
     a[i] = a[pick];
     a[pick] = temp;
   }
} 

// if screenClean is supposed to "clean" the screen
// alpha should always be to the max
void screenClean(float hue,
                 float saturation,
                 float brightness,
                 float alpha,
                 float flickrAmount,
                 float flickrSpeed)
{
  hue = map(hue, 0, 127, 0, 255);
  saturation = map(saturation, 0, 127, 0, 255);
  brightness = map(brightness, 0, 127, 0, 255);
  alpha = map(alpha, 0, 127, 0, 255);

  flickrAmount = map(flickrAmount, 0, 127, 0, 1);
  flickrSpeed = map(flickrSpeed, 0, 127, 1, 120);
  if ((frameCount % int(flickrSpeed)) == 0) {
    brightness = brightness * flickrAmount;
  }
  //println(frameCount + " " + flickrSpeed + " " + (frameCount % flickrSpeed)); 
  
  colorMode(HSB);
  color c = color(hue, saturation, brightness, alpha);

  // flickr? frameCount % var, amount towards black
  
  fill(c);
  noStroke();
  rectMode(CORNER);
  rect(0, 0, width * 2, height);
}



void displayFps(boolean render) { // -----------------------------------
  if (render){
    // Display Fps
    //fill(0); noStroke(); rect(width-80, 10, 80, 30, 4);
    fill(255);
    textFont(fpsFont);
    textSize(16);
    text(round(frameRate) + "fps", width-60, 20, 5);}
}


void autoSnap(boolean render) { // -------------------------------------
  if (render) {
    // auto-save snapshots
    if (frameCount == 1000) {
      saveFrame("../snapshots/"
                + year() + nf(month(),2)
                + nf(day(),2)
                + nf(hour(),2)
                + nf(minute(),2)
                + nf(second(),2)
                + "_.png");
    }
  }
}





// SPHERES -----------------------------------------------------------
//offscreen render spheres

void drawSpheres( boolean render
                  )
{
  if (render) {

    /*  
        sphere.beginDraw();
        sphere.blendMode(BLEND);
        sphere.rect(0, 0, width, height);
        sphere.blendMode(DARKEST);
        sphere.noStroke();
        sphere.pushMatrix();
        sphere.translate(width/2+(width/8), height/2, 580);
        sphere.rotateX(radians(float(frameCount) * 0.1));
        sphere.rotateY(radians(float(frameCount) * 0.23));
        sphere.rotateZ(radians(float(frameCount) * 0.2));
        textureSphere(sphere, 400, 400, 400, imgPool[3][1]);
        sphere.popMatrix();

        sphere.pushMatrix();
        sphere.translate(width/2+(width/8), height/2, 580);
        sphere.rotateX(radians(float(frameCount) * 0.2));
        sphere.rotateY(radians(float(frameCount) * 0.13));
        sphere.rotateZ(radians(float(frameCount) * 0.01));
        textureSphere(sphere, 390, 390, 390, imgPool[3][1]);
        sphere.popMatrix();

        sphere.endDraw();


        //  tint(0);
        //  image(sphere, 0, 0);

        textureWrap(REPEAT);
        //textureMode(NORMAL);

        pushMatrix();
        //tint(255);
        translate(width/2, height/2); // center coordinate system
        translate(((frameCount*1) % width) - width/2,0);
        //stroke(0,50);
        //strokeWeight(1);
        noStroke();

        float quadW = 500;
        float quadH = height;
  
        float v1x = 0; // sin(radians(frameCount))*250+400;

        pushMatrix() ;
        //rotate(PI/PI);

        beginShape(QUADS);
        texture(sphere);
        vertex(-quadW/2, -quadH/2,      0,     0);
        vertex( quadW/2, -quadH/2,  quadW,    0);
        vertex( quadW/2,  quadH/2,  quadW, quadH);
        vertex(-quadW/2,  quadH/2, 0,     quadH);
        endShape();
        popMatrix();
        popMatrix();

    */

  }
}







// SPHERES  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// A 3D textured sphere with simple rotation control.
// thanks to https://processing.org/examples/texturesphere.html
// Texture Sphere written by Gillian Ramsay to better display the poles.
// Previous version by Mike 'Flux' Chang (and cleaned up by Aaron Koblin).
// Original based on code by Toxi.
// modified to accomodate offscreen PGraphics

void initializeSphere(int numPtsW, int numPtsH_2pi) {

  // The number of points around the width and height
  numPointsW=numPtsW+1;
  numPointsH_2pi=numPtsH_2pi;  // How many actual pts around the sphere (not just from top to bottom)
  numPointsH=ceil((float)numPointsH_2pi/2)+1;  // How many pts from top to bottom (abs(....) b/c of the possibility of an odd numPointsH_2pi)

  coorX=new float[numPointsW];   // All the x-coor in a horizontal circle radius 1
  coorY=new float[numPointsH];   // All the y-coor in a vertical circle radius 1
  coorZ=new float[numPointsW];   // All the z-coor in a horizontal circle radius 1
  multXZ=new float[numPointsH];  // The radius of each horizontal circle (that you will multiply with coorX and coorZ)

  for (int i=0; i<numPointsW ;i++) {  // For all the points around the width
    float thetaW=i*2*PI/(numPointsW-1);
    coorX[i]=sin(thetaW);
    coorZ[i]=cos(thetaW);
  }
  
  for (int i=0; i<numPointsH; i++) {  // For all points from top to bottom
    if (int(numPointsH_2pi/2) != (float)numPointsH_2pi/2 && i==numPointsH-1) {  // If the numPointsH_2pi is odd and it is at the last pt
      float thetaH=(i-1)*2*PI/(numPointsH_2pi);
      coorY[i]=cos(PI+thetaH); 
      multXZ[i]=0;
    } 
    else {
      //The numPointsH_2pi and 2 below allows there to be a flat bottom if the numPointsH is odd
      float thetaH=i*2*PI/(numPointsH_2pi);

      //PI+ below makes the top always the point instead of the bottom.
      coorY[i]=cos(PI+thetaH); 
      multXZ[i]=sin(thetaH);
    }
  }
}

void textureSphere(PGraphics pg, float rx, float ry, float rz, PImage t) { 
  // These are so we can map certain parts of the image on to the shape 
  float changeU=t.width/(float)(numPointsW-1); 
  float changeV=t.height/(float)(numPointsH-1); 
  float u=0;  // Width variable for the texture
  float v=0;  // Height variable for the texture

  pg.beginShape(TRIANGLE_STRIP);
  pg.texture(t);
  for (int i=0; i<(numPointsH-1); i++) {  // For all the rings but top and bottom
    // Goes into the array here instead of loop to save time
    float coory=coorY[i];
    float cooryPlus=coorY[i+1];

    float multxz=multXZ[i];
    float multxzPlus=multXZ[i+1];

    for (int j=0; j<numPointsW; j++) {  // For all the pts in the ring
      pg.normal(coorX[j]*multxz, coory, coorZ[j]*multxz);
      pg.vertex(coorX[j]*multxz*rx, coory*ry, coorZ[j]*multxz*rz, u, v);
      pg.normal(coorX[j]*multxzPlus, cooryPlus, coorZ[j]*multxzPlus);
      pg.vertex(coorX[j]*multxzPlus*rx, cooryPlus*ry, coorZ[j]*multxzPlus*rz, u, v+changeV);
      u+=changeU;
    }
    v+=changeV;
    u=0;
  }
  pg.endShape();
}





