/*

	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  switchBlendMode.pde
                                                                             */

void switchBlendMode(int blendMode) 
{
  switch(blendMode) {
  case 0:
    blendMode(ADD); // additive blending with white clip: C = min(A*factor + B, 255)
    break;
  case 1:
    blendMode(SUBTRACT); // subtractive blend w/ black clip: C = max(B - A*factor, 0)
    break;
  case 2:
    blendMode(LIGHTEST); // only the lightest colour succeeds: C = max(A*factor, B)
    break;
  case 3:
    blendMode(DARKEST); // only the darkest colour succeeds: C = min(A*factor, B)    
    break;
  case 4:
    blendMode(SCREEN); // opposite multiply, uses inverse values of the colors.
    break;
  case 5:
    blendMode(MULTIPLY); // multiply the colors, result will always be darker.
    break;
  case 6:
    blendMode(EXCLUSION); // similar to DIFFERENCE, but less extreme.
    break;
  case 7:
    blendMode(REPLACE); // pixels entirely replace others + don't utilize alpha values
    break;
  default:
    blendMode(BLEND); // linear interp. of colours: C = A*factor + B. Default.
    break;
  } // DIFFERENCE: subtract colors from underlying image. NOT SUPPORTED BY P3D!!  */
}

