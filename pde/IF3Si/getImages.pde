/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  getImages.pde
                                                                             */

// loads all images into an array - - - - - - - - - - - - - - - - - - - - - - - - - 
PImage[][] getImages(String folder) {
//ArrayList<ArrayList> getImages(String folder) {

  PImage[][] imgPool; // declare 2D array imgPool
  //ArrayList<ArrayList>  imgPool;

  
  File dir = new File(dataPath(sketchPath + folder)); // first folder
  String[] dirlist = dir.list(); // an array of folders (strings)
  dirlist = sort(dirlist); //
  //imgPool = new ArrayList<ArrayList>();
  imgPool = new PImage[dirlist.length][100]; // create 2d array imgPool
  
  for (int i = 0; i < dirlist.length; i++) {
    String fulldir = dataPath(sketchPath + folder + dirlist[i]) + "/";
    File dir2 = new File(fulldir);
    String[] filelist = dir2.list(); // an array of image names
    filelist = sort(filelist);
    println("\n~~~ BANK no." + i + ": " + dirlist[i]);
    
    if (filelist.length != 0) {
      imgPool[i] = (PImage[]) expand(imgPool[i], filelist.length);
      //     geez: ^^^^^^^^^^ !!!!!

      for (int j = 0; j < filelist.length; j++) {
        println("    imgPool[" + i + "][" + j + "]: " + dirlist[i] + " " + filelist[j]);
        imgPool[i][j] = loadImage(fulldir + filelist[j]);
      }
      
    } else {
      println("No files in this folder: " + fulldir);
    }    
  }

  println("\n~~~ Done loading images.\n");

  return imgPool;
  
}

