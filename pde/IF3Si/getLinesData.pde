/*
  
	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  getLinesData.pde - process spectral lines
  
*/

ArrayList<FloatList> getLinesData() {

  String[] siLinesData;
  float[][] colors = { // these are taken from http://astro.u-strasbg.fr/~koppen/discharge/discharge.html
                { 3800.0,        0,   0,   0},  
                { 4000.0,      150,   0, 150},  
                { 4400.0,      120,   0, 255},  // violet
                { 4500.0,        0,   0, 255},  // blue
                { 4800.0,        0, 255, 255},  // cyan
                { 5200.0,        0, 255,   0},  // green
                { 5800.0,      255, 255,   0},  // yellow
                { 6500.0,      255,   0,   0},  // red
                { 7000.0,       80,   0,   0},
                { 7300.0,        0,   0,   0} };

  siLinesData = loadStrings(dataPath(sketchPath + "/silicon_lines.txt"));
  ArrayList<FloatList> siLinesList = new ArrayList<FloatList>();  

  for (String lineData : siLinesData ) {

    String[] datatemp = split(lineData, " ");
    FloatList data_x;
    data_x = new FloatList();

    // clean up from that messy split
    for (int i = 0; i < datatemp.length; i++) {
      if (!datatemp[i].equals("")) { // if not empty (spaces):
        data_x.append(float(datatemp[i]));
      }
    }

    // iterate through colors and find RGB values for each line
    float wavelength = data_x.get(0);
    for (int i = 0; i < colors.length; i++){
      float thisboundary = colors[i][0];
      if (i+1 < colors.length){
        float nextboundary = colors[i+1][0];
        if(wavelength > thisboundary && wavelength < nextboundary) {
          int red   = round(colors[i][1] + ( ( (colors[i+1][1] - colors[i][1]) / (colors[i+1][0] - colors[i][0]) )
                                               * (wavelength - colors[i][0]) ));
          int green = round(colors[i][2] + ( ( (colors[i+1][2] - colors[i][2]) / (colors[i+1][0] - colors[i][0]) )
                                               * (wavelength - colors[i][0]) ));
          int blue  = round(colors[i][3] + ( ( (colors[i+1][3] - colors[i][3]) / (colors[i+1][0] - colors[i][0]) )
                                               * (wavelength - colors[i][0]) ));
          // add them to the array
          data_x.append(red);
          data_x.append(green);
          data_x.append(blue);
        }
      }
    }
    siLinesList.add(data_x);
  }
  return siLinesList;
}
