/*

	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  testPicture.pde
                                                                             */


void testPicture(boolean render) {
  if (render) {

    blendMode(BLEND);
    fill(127);
    stroke(255);
    strokeWeight(4);

    rectMode(CORNER);
    rect(1, 1, width-2, height-2);

    ellipse(width/2, height/2, height-4, height-4);

    ellipse(height/4, height/4, height/2-4, height/2-4);
    ellipse(height/4, 3*height/4, height/2-4, height/2-4);
    ellipse(width-height/4, height/4, height/2-4, height/2-4);
    ellipse(width-height/4, 3*height/4, height/2-4, height/2-4);

    line(width/2, 0, width/2, height);
    line(0, height/2, width, height/2);

    fill(0);
    textFont(testFont);
    textSize(50);
    textAlign(CENTER,CENTER);

    text("Interface Fractures III", width/2, height/2,4);

    /* more tests:

       - horizontal and vertical block, moving fast!
       - flicker test, black&white, 60Hz
       - color and gray stripes of interpolation


     */

  }
}

