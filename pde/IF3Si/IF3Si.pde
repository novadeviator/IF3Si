/*

	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  IF3Si.pde
    
                                                                             */


// undecorate window (remove window borders etc)  - - - - - - - - - - - - - - -
public void init() { frame.removeNotify(); frame.setUndecorated(true);
   frame.addNotify(); super.init(); } 

// load libs
import oscP5.*; // Open Sound Control
import netP5.*;


// declarations - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// declare OSC object
OscP5 oscP5;


// IMAGE POOL, a 2D array
PImage[][] imgPool; 

// texture (tiles)
float texX = 0;
float texY = 0;

// generate an array of random numbers
int[] rands = new int[500];
IntList randz; // arrayList

// for draw cube
int cubesnum = 20;
PGraphics[] cubes = new PGraphics[cubesnum];
PShader blur;
PGraphics bpass1, bpass2;

// spheres
int ptsW, ptsH;

int numPointsW;
int numPointsH_2pi; 
int numPointsH;

float[] coorX;
float[] coorY;
float[] coorZ;
float[] multXZ;

PGraphics sphere;

// tiles
boolean drawTilesToggle;
int tilesBgHue, tilesBgSat, tilesBgBri, tilesHue, tilesSat, tilesBri;
int tilesNumX, tilesNumY, tilesTexBank, tilesTexId;
float tilesTexSpeedX, tilesTexSpeedY, tilesOverlap;

// testPicture
boolean testPictureToggle = false;
PFont testFont;

// testPattern
boolean testPatternToggle = false;
int testPatternTexBank, testPatternTexId, testPatternTexAlpha;

// fps
PFont fpsFont;


// siLines
ArrayList<FloatList> siLinesData;

// screenClean
float screenCleanHue, screenCleanSaturation, screenCleanBrightness,
  screenCleanAlpha, screenCleanFlickrAmount, screenCleanFlickrSpeed;

// drawSpectrum
boolean drawSpectrumToggle;
float drawSpectrumAFactor;
int drawSpectrumThreshold;
int drawSpectrumHeight;
int drawSpectrumWidth;
float drawSpectrumAwidth;
float drawSpectrumSaturation;




///////////////////////////////////////////////////////////////////////////////
void setup() { // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  float sizefactor = 1; // define the size of the screen, 1 = 1080p
  size( int( 1920 * sizefactor ),
        int( 1080 * sizefactor ),
        P3D ); // renderer
  
  // framerate
  frameRate(60);
  smooth(32); // 32??
  noCursor();
  background(0);

  println("\n\n~~~ Hello. Starting Interface Fractures III - SILICON." +
          " - - - - - - - - - - - - - - - - - - - - - -\n");

  
  // open sound control
  oscP5 = new OscP5(this,12000); // listening at port 12000
  println("~~~ starting oscP5 ...");
  oscP5.plug(this,"ctlin","/ctlin"); // osc from Renoise/Midi (via SC) -> function 'ctlin'
  oscP5.plug(this,"scosc","/sc"); // osc from SuperCollider -> function 'scosc'

  // get all textures into an image pool
  println("\n\n~~~ loading textures into image pool ...\n");
  imgPool = getImages("/images/");




  // ----------------------------------------------------------------------
  println("~~~ getting and processing lines data ...");
  //siLinesData = new ArrayList<FloatList>();
  siLinesData = getLinesData();  // function, returns an ArrayList
  //printArray(siLinesData);


  // ----------------------------------------------------------------------

  
  // create an array of random value between -50 and 50
  for (int i=0; i < 500; i++) { rands[i] = i-250; }
  shuffle(rands); 


  // drawCube ` ` ` ` ` ` ` ` ` ` ` ` ` ` ` ` ` ` `
  // an array of PGraphics
  for (int i = 0; i < cubesnum; i++) {
    cubes[i] = createGraphics(width, height, P3D);
  }
  blur = loadShader("blur.glsl");
  bpass1 = createGraphics(width, height, P3D);
  bpass1.smooth();
  bpass2 = createGraphics(width, height, P3D);
  bpass2.smooth();


  randz = new IntList(width);
  for (int i=0; i < width; i++) {
    randz.set(i, i);
  }
  randz.shuffle(); 
  //println(randz);

  // spheres
  ptsW=30;
  ptsH=30;
  initializeSphere(ptsW, ptsH);   // number of vertices around the width and height
  sphere = createGraphics(width, height, P3D);

  // for testPicture
  //String[] fontList = PFont.list();
  //printArray(fontList);
  testFont = createFont("Oliver's Barney", 50);

  // fps
  fpsFont = createFont("Ubuntu Mono", 16);


  


}




//////////////////////////////////////////////////////////////////////////////
void draw() { // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  // SCENENGINES / / / / / / / / / / / / / / / / / / / / / / / / / / |

  // clean screen ````````````````````````````````````````````````````|
  blendMode(BLEND);
  screenClean(screenCleanHue,
              screenCleanSaturation,
              screenCleanBrightness,
              screenCleanAlpha,
              screenCleanFlickrAmount,
              screenCleanFlickrSpeed);

  // drawSpectrum
  drawSpectrum(drawSpectrumToggle,
               siLinesData, // ArrayList<FloatList> 2D data
               drawSpectrumThreshold,	// threshold 0-1000 (1000 = nothing)
               drawSpectrumAFactor,		// alpha factor (58 = 1)
               drawSpectrumHeight,		// line height
               drawSpectrumWidth,			// line height
               drawSpectrumAwidth,  		// alpha->width amp
               drawSpectrumSaturation  		// saturation
               );


  // draw tiles `````````````````````````````````````````````````````|
  tiles(drawTilesToggle,               // render on/off
        tilesBgHue, tilesBgSat, tilesBgBri, tilesHue, tilesSat, tilesBri,
        //color(0, 0, 0, 80), // background color (HSBA)
        // color(80, 70, 20, 100), // tile color
        20,                 // tile hue distance
        0, // blendMode
        tilesNumX,                // number of tiles on X axis
        tilesNumY, // number of tiles on Y axis
        tilesTexBank,  // texture bank number
        tilesTexId,  // texture number/id
        tilesTexSpeedX, // texture speed X
        tilesTexSpeedY, // texture speed Y
        tilesOverlap // overlap. 127 = 300%
        );

  
  // draw spheress```````````````````````````````````````````````````|
  drawSpheres(boolean(0)
              );





  
  // draw cubes `````````````````````````````````````````````````````|
  drawCube(boolean(0), cubes,
           100, height/2, -100,
           400, 300, 300,
           radians(frameCount), radians(frameCount*0.7), PI/2,
           0);



  // test pattern```````````````````````````````````````````````````|
  

  testPattern(testPatternToggle, // boolean(0), // on/off
              testPatternTexBank, testPatternTexId,
              testPatternTexAlpha, // image alpha
              
              10, // number of horizontal 'lanes'
              10,  // density
              4, // stroke width
              255, // stroke alpha
              2 // speed
              );


  // debug `````````````````````````````````````````````````````````|
  // draw test picture
  testPicture(testPictureToggle);
  
  // frames per second
  displayFps(false);
  
  // document
  autoSnap(false);
  
} // --------------------------------------------------------------------------



