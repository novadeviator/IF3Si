/*

	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  testPattern.pde
    
                                                                             */

/*
  TO BE ADDED:
     - use quads
     - add textures
    >- more TO BE EXPANDED / REFINED!!!
*/

void testPattern( boolean render,
                  int imgBank, int imgID,
                  int imgAlpha,
                  int ylines,
                  int density,
                  int lwidth,
                  int lalpha,
                  int speed
                  )
{
  if (render) {

    tint(imgAlpha);
    image(imgPool[imgBank][imgID], 0, 0);

    blendMode(BLEND);
  
    int yheight = height/ylines;
  
    for (int y=0; y < ylines; y++) {
      int yloc = int((y+1)*height/(ylines+1));
      for (int x=0; x < width; x++) {

        int index = x + (y * (width/ylines));
        index = index % width;
        if (randz.get(index) < density * 0.01 * randz.get((index + x + y) % randz.size())) {
        
          int fx = (x + frameCount); // move!
          fx = fx * speed * 1  * randz.get((abs(index - x + y) % randz.size())) / randz.size(); // speed!
          fx %= width; // wrap at the right edge
      
          color imgC = imgPool[imgBank][imgID].get(fx, yloc);
          stroke(imgC, lalpha);
          strokeWeight(1 + map(brightness(imgC), 0, 255, 0, lwidth));

          line(fx, yheight * y + (yheight/30), fx, yheight * (y+1) - (yheight/30));
        }
      }
    }
  }
}


