/*
  
	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  drawSpectrum.pde - draw spectral lines
  
*/



void drawSpectrum(boolean render,
                  ArrayList<FloatList> siLinesData,
                  int threshold,
                  float afactor,
                  int line_height,
                  int rect_width,
                  float alpha_width,
                  float saturation)
{
  if (render) {
    // if((frameCount%60) < 1) { println("width: " + width); }; // DEBUG
    
    //float afactor = float(afactor);
    afactor = map(pow(afactor, 5), 0, pow(127, 5), 0, 50 );
    //line_height = int(map(line_height, 0, 127, 0, height/2));
    threshold = int(map(pow(threshold, 3), 0, pow(127, 3), 0, 1000));
    //if((frameCount%60) < 1) { println("line_height: " + line_height); }; // DEBUG    
		line_height = int(map(pow(line_height,2), 0, pow(127,2), 0, height));
    rect_width = int(map(rect_width, 0, 127, 1, 400));
    alpha_width = map(alpha_width, 0, 127, 0, 0.1);
    saturation = map(saturation, 0, 127, 0, 255);
    // DEBUG:
    //if((frameCount%60) < 1) { println("line_height: " + line_height); }; // DEBUG
      
    for (int i = 0; i < siLinesData.size(); i++) {
      if (siLinesData.get(i).get(1) > threshold) {
        float wavelength = siLinesData.get(i).get(0);
        float intensity = siLinesData.get(i).get(1);
        float red = siLinesData.get(i).get(2);
        float green = siLinesData.get(i).get(3);
        float blue = siLinesData.get(i).get(4);
        float alpha = map(intensity, 0, 1000, 0, 255) * afactor;
        colorMode(RGB);
        color line_c = color(red, green, blue, alpha);
        colorMode(HSB);
        float hue = hue(line_c);
        float brightness = brightness(line_c);
        line_c = color(hue, saturation, brightness, alpha);
        


        fill(line_c); noStroke();
        rectMode(CENTER);
        rect(map(wavelength, 3950, 6741, 1, width), height / 2, rect_width + (alpha * alpha_width), line_height );


        // stroke(line_c);
        // line(map(wavelength, 3950, 6741, 1, width), height/2 - line_height,
        //     map(wavelength, 3950, 6741, 1, width), height/2 + line_height);
      }
    }

  }
}
