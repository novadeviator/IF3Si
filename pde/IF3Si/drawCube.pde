/*

	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  drawCube.pde
    
                                                                             */

void drawCube( boolean render, PGraphics cubes[],
               int posX, int posY, int posZ,
               int dimW, int dimH, int dimL,
               float rotX, float rotY, float rotZ,
               int blurAmount
               //float xd, float yd
             )
{
  if (render) {

    blendMode(BLEND);
    fill(0);
    rect(0, 0, width, height);
    //blendMode(LIGHTEST);
    for (int i=0; i < cubes.length; i++) {

      posX += 100;
  
      cubes[i].beginDraw();
      cubes[i].clear();
      cubes[i].smooth();
      cubes[i].lights();

      cubes[i].stroke(255);
      cubes[i].strokeWeight(10);
      cubes[i].fill(127);
      //cubes[i].noFill();
      cubes[i].translate(posX, posY, posZ);
      cubes[i].rotateX(rotX);
      cubes[i].rotateY(rotY);
      cubes[i].rotateZ(rotZ);
      cubes[i].box(dimW, dimH, dimL);
      cubes[i].endDraw();

      if (blurAmount != 0) { // this blur chokes graphic processor
  
        blur.set("blurSize", blurAmount);
        blur.set("sigma", 9.0f);

        blur.set("horizontalPass", 0);
        bpass1.beginDraw();
        bpass1.clear();
        //bpass1.noLights();
        bpass1.shader(blur);
        bpass1.image(cubes[i], 0, 0);
        bpass1.endDraw();

        blur.set("horizontalPass", 1);
        cubes[i].beginDraw();
        cubes[i].clear();
        //cubes[i].noLights();
        cubes[i].shader(blur);
        cubes[i].image(bpass1, 0, 0);
        cubes[i].endDraw();
      }

      image(cubes[i], 0, 0);

    }
  }
}

