
Structure of code
-----------------

Main components of this project are three:
* Processing code taking care of visuals (in folder pde/)
* SuperCollider code - generative and live sound, GUI interface for touchscreen (in folder scd/)
* Renoise module - composition (sound) and midi triggers for Processing/visuals (in foder rnx/)

Helper folders:
* dat/ contains some data for SuperCollider (for now)
* smp/ contains samples for SuperCollider
* doc/ contains some simple documentation (this file!)
* dev/ is some development code (may not be in git)
* textures for Processing are in pde/IF3Si/images and may not be in git)

How is this run?
================

Hardware setup contains of a computer running a capable OpenGL card (Nvidia?) with two displays. One is a multitouch display device (1920x1080 but not necessary), the other is either a simple display monitor or a projector capable of 60fps at FullHD (1080p). Underlaying OS is Linux.

## Software

Edit `~/.processing/preferences.txt` and change `run.display=1` accordingly. I have it on 1, since 0 is my 'primary' display, which is the touchscreen. See also an example xorg.conf. I personally do the following - I access everything through a remote laptop, loging in through ssh. Processing applet is run remotely by using two small bash scripts:

*processing-runsketch.sh* (this one needs to be on the executable path):
```bash
#!/bin/bash
if [[ -z $1 ]]; then
	echo "error. no argument."
	echo "need the sketch path"
else

export DISPLAY=":0"
~/src/processing-2.2.1/processing-java \
    --force \
    --sketch=$1 \
    --run \
    --output=$1/output

fi
```

and 

*processing-runsketch_IF3.sh*:

```bash
#!/bin/bash
while (true)
do
    # clear
    processing-runsketch.sh ~/PROJECTS/IF3Si/pde/IF3Si/
    echo	
    echo "~~~ Program has quit. - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
    echo
done

```

### SuperCollider

*IF3supercollider.sh*:
```bash
#!/bin/bash
export DISPLAY=":0.0"
/usr/local/bin/sclang ~/PROJECTS/IF3Si/scd/IF3Si.scd 

```

**Renoise sends MIDI to SuperCollider who controls Processing patch via OSC.**

## notes

Monitor files while developing to restart Processing sketch on save with [`entr`](http://entrproject.org/). After running the sketch in one terminal with `processing-runsketch_IF3.sh`, in another terminal run something like 
```
ls PROJECTS/IF3Si/pde/IF3Si/*.pde | entr killall java
```
(this will kill other running instances of java, so beware)









.






