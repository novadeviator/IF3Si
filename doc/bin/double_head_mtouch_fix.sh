#!/bin/bash

# use this when your touch-screen is on the left (coordinates 0 0)
# and your other (non-touch) is one the right
export DISPLAY=:0
xinput set-prop "Advanced Silicon S.A CoolTouch(TM) System" --type=float "Coordinate Transformation Matrix" 0.5 0 0 0 1 0  0 0 1
